module.exports = function(grunt) {
    
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Define reusable paths.
        paths: {
            app: 'app',
            dist: 'dist',
            app_css: '<%= paths.app %>/css',
            app_js: '<%= paths.app %>/js',
            source_scss: '<%= paths.app %>/src/scss',
            source_js: '<%= paths.app %>/src/js',
            source_bower: '<%= paths.app %>/src/bower',
            dist_css: '<%= paths.dist %>/css',
            dist_js: '<%= paths.dist %>/js',
            dist_img: '<%= paths.dist %>/images'
        },

        sass: {
            dev: {
                options: {
                    outputStyle: 'expanded',
                    sourceMap: false
                },
                files: {
                    '<%= paths.app_css %>/styles.css': '<%= paths.source_scss %>/app.scss'
                }
            },
            build: {
                options: {
                    outputStyle: 'compressed',
                    sourceMap: false
                },
                files: {
                    '<%= paths.dist_css %>/styles.css': '<%= paths.app_css %>/styles.css'
                }
            }
        },

        browserSync: {
            files: {
                src: ['<%= paths.app_css %>/*.css', '<%= paths.app %>/*.html', '<%= paths.app_js %>/*.js']
            },
            options: {
                browser: 'google chrome',
                server: '<%= paths.app %>',
                watchTask: true
            }
        },

        watch: {
            sass: {
                files: ['<%= paths.source_scss %>/**/*.scss'],
                tasks: ['sass:dev', 'concat:css']
            },
            js: {
                files: ['<%= paths.source_js %>/*.js'],
                tasks: ['jshint', 'uglify:dev']
            }
        },

        jshint: {
            dev: {
                files: {
                    src: '<%= paths.source_js %>/*.js'
                }
            },
            options: {
                reporter: require('jshint-stylish')
            }
        },

        bower: {
            dev: {
                dest: '<%= paths.source_bower %>',
                js_dest: '<%= paths.source_bower %>/js',
                css_dest: '<%= paths.source_bower %>/styles'
            }
        },
        
        concat: {
            css: {
                src: ['<%= paths.app_css %>/styles.css', '<%= paths.source_bower %>/styles/**/*.css'],
                dest: '<%= paths.app_css %>/styles.css'
            }
        },

        uglify: {
            dev: {
                options: {
                    beautify: true,
                    mangle: false,
                    compress: false,
                    preserveComments: 'all'
                },
                src: ['<%= paths.source_js %>/*.js', '<%= paths.source_bower %>/js/**/*.js'],
                dest: '<%= paths.app_js %>/scripts.js'
            },
             build: {
                 src: ['<%= paths.source_js %>/*.js', '<%= paths.source_bower %>/js/**/*.js'],
                 dest: '<%= paths.dist_js %>/scripts.min.js'
             }
        },

        copy: {
            html: {
                expand: true,
                cwd: '<%= paths.app %>',
                src: '*.html',
                dest: '<%= paths.dist %>/',
                options: {
                    process: function(content, srcpath){
                        return content.replace('scripts.js', 'scripts.min.js');
                    }
                }
            }
        },

        clean: {
            dist: {
                src: '<%= paths.dist %>'
            }
        },

        imagemin: {
            build: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= paths.app_img %>',
                        src: ['**/*.{png,jpg,gif,svg,ico'],
                        dest: '<%= paths.dist_img %>'
                    }
                ]
            }
        }
    });

    // Load the plugins.

    // BrowserSync.
    grunt.loadNpmTasks('grunt-browser-sync');

    // Sass.
    grunt.loadNpmTasks('grunt-sass');

    // Watch.
    grunt.loadNpmTasks('grunt-contrib-watch');

    // JSHint.
    grunt.loadNpmTasks('grunt-contrib-jshint');

    // Bower.
    grunt.loadNpmTasks('grunt-bower');

    // Concat.
    grunt.loadNpmTasks('grunt-contrib-concat');

    // Uglify.
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // Copy.
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Clean.
    grunt.loadNpmTasks('grunt-contrib-clean');

    // Imagemin.
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    
    // Create Tasks.

    // Default task.
    grunt.registerTask('default', ['browserSync', 'watch']);

    // Build.
    grunt.registerTask('build', ['clean:dist', 'copy', 'imagemin', 'uglify:build', 'concat:css', 'sass:build']);
}